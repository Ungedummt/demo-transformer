#include <gtk/gtk.h>


#define SCALE_FACTOR 2


#define DEMO_TYPE_TRANSFORMER (demo_transformer_get_type ())
G_DECLARE_FINAL_TYPE (DemoTransformer, demo_transformer, DEMO, TRANSFORMER, GtkWidget)

/* Declare the struct. */
struct _DemoTransformer
{
  GtkWidget parent_instance;

  GtkWidget *child;
};

struct _DemoTransformerClass
{
  GtkWidgetClass parent_class;
};

static GtkWidget *
demo_transformer_new (GtkWidget *child)
{
  DemoTransformer *result;

  result = g_object_new (DEMO_TYPE_TRANSFORMER,
                         NULL);
  result->child = child;
  gtk_widget_set_parent (child, GTK_WIDGET (result));

  return GTK_WIDGET (result);
}

G_DEFINE_TYPE (DemoTransformer, demo_transformer, GTK_TYPE_WIDGET)

static void
demo_transformer_measure (GtkWidget      *widget,
                          GtkOrientation  orientation,
                          int             for_size,
                          int            *minimum,
                          int            *natural,
                          int            *minimum_baseline,
                          int            *natural_baseline)
{
  DemoTransformer *self = DEMO_TRANSFORMER (widget);

  gtk_widget_measure (self->child,
                      orientation,
                      for_size > 0 ? for_size * SCALE_FACTOR : for_size,
                      minimum,
                      natural,
                      NULL, NULL);

  /* round up */
  *minimum = (*minimum + SCALE_FACTOR - 1) / SCALE_FACTOR;
  *natural = (*natural + SCALE_FACTOR - 1) / SCALE_FACTOR;
}

static void
demo_transformer_size_allocate (GtkWidget *widget,
                                int        width,
                                int        height,
                                int        baseline)
{
  DemoTransformer *self = DEMO_TRANSFORMER (widget);

  gtk_widget_allocate (self->child,
                       width * SCALE_FACTOR,
                       height * SCALE_FACTOR,
                       -1,
                       gsk_transform_scale (NULL, 1.f / SCALE_FACTOR, 1.f / SCALE_FACTOR));
}

static void
demo_transformer_class_init (DemoTransformerClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  widget_class->measure = demo_transformer_measure;
  widget_class->size_allocate = demo_transformer_size_allocate;
}

static void
demo_transformer_init (DemoTransformer *peg)
{
}

int
main (int argc, char *argv[])
{
  GtkWidget *win, *transformer, *label;
  GListModel *toplevels;

  gtk_init ();

  win = gtk_window_new ();

  label = gtk_label_new ("Hello World");

  transformer = demo_transformer_new (label);
  gtk_window_set_child (GTK_WINDOW (win), transformer);

  gtk_widget_show (win);

  toplevels = gtk_window_get_toplevels ();
  while (g_list_model_get_n_items (toplevels))
    g_main_context_iteration (NULL, TRUE);

  return 0;
}
