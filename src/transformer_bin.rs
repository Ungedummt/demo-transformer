use std::cell::Cell;
use gtk::{
    glib, gsk,
    prelude::*,
    subclass::prelude::*,
};


mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct TransformerBin {
        pub zoom: Cell<f64>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TransformerBin {
        const NAME: &'static str = "TransformerBin";
        type ParentType = gtk::Widget;
        type Type = super::TransformerBin;
        type Interfaces = ();

        fn new() -> Self {
            Self {
                zoom: Cell::new(5.),
            }
        }
    }

    impl WidgetImpl for TransformerBin {
        fn measure(&self, widget: &Self::Type, orientation: gtk::Orientation, for_size: i32) -> (i32, i32, i32, i32) {
            let zoom = self.zoom.get();
            let new_for_size;

            if for_size > 0 {
                new_for_size = (zoom * for_size as f64) as i32;
            } else {
                new_for_size = for_size;
            }

            let (minimum, natural, _, _) = widget.first_child().unwrap().measure(orientation, new_for_size);

            ((minimum as f64 * zoom) as i32, (natural as f64 * zoom) as i32, -1, -1)
        }

        fn size_allocate(&self, widget: &Self::Type, width: i32, height: i32, baseline: i32) {
            let zoom = self.zoom.get() as f32;

            let transform = gsk::Transform::new().scale(zoom, zoom).unwrap();

            widget.first_child().unwrap().allocate((width  as f32 / zoom) as i32,
                                                   (height as f32 / zoom) as i32,
                                                    baseline, Some(&transform));
        }
    }

    impl ObjectImpl for TransformerBin {
        fn constructed(&self, obj: &Self::Type) {
            obj.set_halign(gtk::Align::Start);
            obj.set_valign(gtk::Align::Start);
        }

        fn dispose(&self, obj: &Self::Type) {
            let child = obj.first_child().unwrap();
            child.unparent();
        }
    }

    impl ScrollableImpl for TransformerBin {}
}

glib::wrapper! {
    pub struct TransformerBin(ObjectSubclass<imp::TransformerBin>)  @extends gtk::Widget;
}

impl TransformerBin {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        let obj: TransformerBin = glib::Object::new(&[]).unwrap();

        let child = gtk::Label::new(Some(&"Hello World!"));
        child.set_parent(&obj);

        obj
    }

    pub fn imp(&self) -> &imp::TransformerBin {
        imp::TransformerBin::from_instance(self)
    }
}
