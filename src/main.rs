mod transformer_bin;

use transformer_bin::TransformerBin;

use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct ExampleApplication {
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ExampleApplication {
        const NAME: &'static str = "ExampleApplication";
        type Type = super::ExampleApplication;
        type ParentType = gtk::Application;
    }

    impl ApplicationImpl for ExampleApplication {
        fn activate(&self, app: &Self::Type) {
            let window = gtk::ApplicationWindow::new(app);
            let child  = TransformerBin::new();
            window.set_child(Some(&child));
            window.present();
        }
    }

    impl ObjectImpl for ExampleApplication {}
    impl GtkApplicationImpl for ExampleApplication {}
}

glib::wrapper! {
    pub struct ExampleApplication(ObjectSubclass<imp::ExampleApplication>)
        @extends gio::Application, gtk::Application,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl ExampleApplication {
    pub fn new() -> Self {
        glib::Object::new(&[])
        .expect("Application initialization failed...")
    }

    pub fn run(&self) {
        ApplicationExtManual::run(self);
    }
}

fn main() {
    gtk::init().expect("Unable to start GTK4");

    let app = ExampleApplication::new();
    app.run();
}
